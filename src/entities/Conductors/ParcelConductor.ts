import {GPSCoordinate} from "../types/GPSCoordinate";
import {MapConductor} from "./MapConductor";
import {SETTINGS} from "../utils";

declare var google: any;


export class ParcelConductor extends MapConductor {
    private points: Array<GPSCoordinate> = [];

    draw(point: GPSCoordinate) {
        this.setMapOnAll(null, this.tempElements);
        this.points.push(point);
        this.tempElements = [];

        var bermudaTriangle = new google.maps.Polygon({

            editable: this.editable,
            draggable: this.draggable,
            clickable: this.clickable,
            paths: this.points,
            ...(this.editable || 1 ? SETTINGS.ACTIVE.POLYGON : SETTINGS.IN_ACTIVE.POLYGON)
        });
        this.tempElements.push(bermudaTriangle);
        this.setMapOnAll(this.map, this.tempElements);
    }

    drawPath(points: Array<GPSCoordinate>) {
        this.setMapOnAll(null, this.tempElements);
        // this.points.push(point);
        this.tempElements = [];

        var bermudaTriangle = new google.maps.Polygon({

            editable: this.editable,
            draggable: this.draggable,
            clickable: this.clickable,
            paths: points,
            ...(this.editable || 1 ? SETTINGS.ACTIVE.POLYGON : SETTINGS.IN_ACTIVE.POLYGON)
        });
        this.tempElements.push(bermudaTriangle);
        this.setMapOnAll(this.map, this.tempElements);
    }
}
