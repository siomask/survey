import Main from './Main';
import {Geometry} from './Geometry';

export class Parcel extends Main {
    wojewodztw: string;
    gmina: string;
    numer: string;
    ownership: string;
    static edit_keys: Array<string> = [
        'wojewodztw',
        'numer',
        'gmina',
    ];

    constructor(data: any = {points: new Geometry()}) {

        super(data);
        this.gmina = data.gmina || '';
        this.numer = data.numer || '';
        this.ownership = data.ownership || '';
        this.wojewodztw = data.wojewodztw || '';
        if (data instanceof Parcel) return data;
    }

    editKeys() {
        return Parcel.edit_keys
    }

    keys() {
        return [
            ...super.keys(),
            ...this.editKeys()
        ];
    }
}
