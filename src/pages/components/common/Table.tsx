/* eslint no-mixed-operators: 0 */
import React, {Component} from 'react';
import './index.scss';

class Table extends Component<{
    columns: Array<any>,
    rowId: string,
    dataSource: any,
},
    {}> {

    renderColumnsContent = (columns: Array<any>, row: any) => {
        return columns.map(({dataIndex, render}) => (
            (
                <td key={dataIndex}>
                    {render && typeof render === 'function'
                        ? render(...dataIndex.split(' ').map((key: any) => row[key]),row)
                        : row[dataIndex]
                    }
                </td>
            )
        ));
    }

    renderTableContent = (dataSource: any) => {
        const {columns = []} = this.props;
        return dataSource.map((row: any) => {
            return(
              <tr key={row[this.props.rowId]}>
                  {this.renderColumnsContent(columns, row)}
              </tr>
            )
        });
    };

    render() {
        const {
            dataSource,
            columns,
        } = this.props;
        return (
            <div className="table-container container-fluid">
                {(
                    <table className="table">
                        <thead className="table__header">
                        <tr key={Math.random()}>
                            {columns.map((column: any) => <th key={column.dataIndex}
                                                              className={`p-5 ${column.class}`}>{column.title}</th>)}
                        </tr>
                        </thead>
                        <tbody className="table-body">{this.renderTableContent(dataSource)}</tbody>
                    </table>
                )
                }
                {dataSource.length === 0
                    ? <div className="no-items-text">No items found</div>
                    : null
                }
            </div>
        );
    }
}

export default Table;
