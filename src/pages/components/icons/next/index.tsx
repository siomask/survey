import React, { Component } from 'react';
import './index.scss';
import IconNext from '../../../../assets/img/icons/left-arrow.png';

class NextIcon extends Component<{ isNext: any },
  {}> {

  render() {
    return (
      <img  className={`arrow-icon ${this.props.isNext?'next-step':''}`} src={IconNext}/>
    );
  }
}


export default NextIcon;
