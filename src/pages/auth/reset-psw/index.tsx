import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {resetPsw, changePsw, signOut, moduleName, userSelector} from '../../../ducks/auth';
import './login.scss';
import {Link, RouteComponentProps, withRouter} from "react-router-dom";
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";

import Loading from "../../../components/loading";
import Logo from "../../../assets/img/logo_BKW1@2x.png";

import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';

interface MatchParams {
    MemberID: string;
}

const ValidF: any = ValidatorForm;

interface Props extends RouteComponentProps<MatchParams> {
    user: any,
    signOut: any,
    changePsw: Function,
    refreshed: any,
    resetPsw: any,
    loading: boolean,
    authError: boolean
}

class ResetPsw extends Component<Props,
    {}> {

    state = {
        pending: false,
        error: false,
        reppassword: '',
        password: '',
    };

    componentDidMount(): void {
        ValidF.addValidationRule('isPasswordMatch', (value: string) => {
            if (value !== this.state.password) {
                return false;
            }
            return true;
        });
    }

    componentWillUnmount() {
        ValidF.removeValidationRule('isPasswordMatch');
    }

    componentWillReceiveProps(nextProps: Readonly<Props>, nextContext: any): void {
        if (nextProps.refreshed !== this.props.refreshed) {
            this.props.history.push("/login");
        }
    }

    onInput = (e: any) => {
        this.setState({
            [e.target.name]: e.target.value,
            error: false,
        });
    };
    onSubmit = async (e: any) => {
        const form: any = this.refs.form;
        form.isFormValid().then(async (valid: any) => {
            if (valid) {
                try {
                    if (this.props.user) {
                        await this.props.changePsw({
                            ...this.props.user.info.data, 
                            password: this.state.password,

                        });
                    } else {
                        await this.props.resetPsw({
                            ...this.state,
                            tempForgotPswLink: location.href.split("?")[1].split("=")[1]

                        });
                    }

                } catch (e) {
                    this.setState({
                        error: e.message || e
                    });
                }
            } else {
                this.setState({
                    disabled: true,
                });
            }
        })


    };
    onSubmitForm = async (e: any) => {
        const form: any = this.refs.form;
        form.submit();
    }

    render() {
        const {authError} = this.props;
        const {error} = this.state;
        return (
            <div className={'first-page d-flex a-c j-c'}>
                <div className={'main-page-view text-center'}>
                    <img className={`logo `} src={Logo}/>
                    <Typography className={'title'} variant="h6" noWrap>
                        Change Password
                    </Typography>

                    <ValidatorForm
                        ref="form"
                        onSubmit={this.onSubmit}
                        onError={(errors: any) => console.log(errors)}
                        className={'d-flex f-col'}
                    >
                        <FormControl>
                            <TextValidator
                                autoComplete={'off'}
                                type={'password'}
                                label={'New Password'}
                                value={this.state.password}
                                name={'password'}
                                onChange={this.onInput}
                                margin="normal"
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </FormControl>
                        <FormControl>
                            <TextValidator
                                autoComplete={'off'}
                                type={'password'}
                                label={'Repeat Password'}
                                value={this.state.reppassword}
                                name={'reppassword'}
                                onChange={this.onInput}
                                margin="normal"
                                validators={['required', 'isPasswordMatch']}
                                errorMessages={['this field is required', 'password mismatch']}
                            />
                        </FormControl>
                    </ValidatorForm>
                    <div className={'d-flex f-col'}>
                        <div className={'d-flex j-end'}>
                            {
                                this.props.user ? null : (<Link to="/" className={'link'}>Back to Login</Link>)
                            }

                        </div>
                        <div className={'d-flex j-center'}>
                            <Button
                                variant="contained"
                                onClick={this.onSubmitForm}
                                className={'my-btn btn-primary'}
                                style={{minWidth: 160}}
                            >
                                CHANGE PASSWORD
                                {this.props.loading && <Loading/>}
                            </Button>
                            {
                                this.props.user ? (
                                    <Button
                                        variant="contained"
                                        onClick={this.props.signOut}
                                        className={'my-btn btn-primary'}
                                        style={{minWidth: 160, marginLeft: 20}}
                                    >
                                        SWITCH ACCOUNT
                                    </Button>
                                ) : null
                            }
                        </div>
                    </div>
                    {
                        authError && (<p style={{color: 'red'}} className={'error-message'}>
                            {authError}
                        </p>)
                    }
                    {
                        error && (<p style={{color: 'red'}} className={'error-message'}>
                            {error}
                        </p>)
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({
    user: userSelector(state),
    refreshed: state[moduleName].refreshed,
    authError: state[moduleName].error,
    loading: state[moduleName].loading,
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        signOut,
        resetPsw,
        changePsw,
    }, dispatch)
);
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ResetPsw));
