import React, {Component} from 'react';

import './index.scss';

/*
export class Table extends Component<{}, { refreshing: boolean }> {

    state = {
        refreshing: false,
    };


    render() {
        return (
            <div className="loader">
                <div className="spinner">
                    <div className="bounce1"></div>
                    <div className="bounce2"></div>
                    <div className="bounce3"></div>
                </div>
            </div>
        );
    }
}*/


import {TableBody, TableCell, TablePagination, withStyles, TableRow, Checkbox} from '@material-ui/core';
import clsx from 'clsx';
import Paper from '@material-ui/core/Paper';
import {AutoSizer, Column, Table} from 'react-virtualized';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import moment from 'moment';
import Tooltip from '@material-ui/core/Tooltip';
import {StyleRulesCallback, WithStyles, createStyles, StyledComponentProps} from "@material-ui/core";


function styles(theme: any) /*: do not put a return type here */ {
    return createStyles({
        flexContainer: {
            display: 'flex',
            alignItems: 'center',
            boxSizing: 'border-box',
        },
        tableRow: {
            cursor: 'pointer',
        },
        tableRowHover: {
            '&:hover': {
                backgroundColor: theme.palette.grey[200],
            },
        },
        tableCell: {
            flex: 1,
        },
        noClick: {
            cursor: 'initial',
        },
    })
}

interface MapProps {
    onRowsRendered: Function,
    hasActions: boolean,
    hasSelection: boolean,
    rowCount: number,
    onDeleteItem: Function,
    onEditItem: Function,
    onSelectAllClick: Function,
    rowGetter: Function,
    rowHeightGetter: Function,
    onRowClick: Function,
    maxWidth: number,
    minWidth: number,
    rowHeight: number,
    rows: any,
    columns: any,
    selected: Array<any>,
    classes: any


}


class MuiVirtualizedTable extends React.PureComponent<MapProps, { isSelected: boolean }> {
    static defaultProps = {
        headerHeight: 24,
        rowHeight: 48,
    };

    constructor(p: any) {
        super(p);
        this.state = {
            isSelected: false,
        };
    }

    getRowClassName = (opt: any) => {
        const {index} = opt;
        const {classes, onRowClick}: any = this.props;

        return clsx(classes.tableRow, classes.flexContainer, {
            [classes.tableRowHover]: index !== -1 && onRowClick != null,
        });
    };
    handleCheck = (event: any, id: any) => {
        const {selected}: any = this.props;
        const selectedIndex = selected.indexOf(id);
        let newSelected: any = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        this.props.onSelectAllClick(newSelected);
    };

    renderCellValue(key: any, item: any, col: any) {
        const val = item[key];

        if (col && col.render) {
            return col.render(item, {...col, key});
        }

        if (['createdAt'].indexOf(key) > -1) {
            const valueDate = moment(val).format('l');
            return (<span>{valueDate}</span>);
        }
        return (
            <span className={'flex text-overflow  align-center'} title={val}>{val}</span>
        );
    }


    cellRenderer = (opt: any) => {
        const {dataKey, columnIndex, rowData, totalIndex}: any = opt;
        const isSelected = this.props.selected.indexOf(rowData.id) !== -1;
        const {columns, classes, rowHeight, onRowClick, hasActions, hasSelection}: any = this.props;
        const {onDeleteItem, onEditItem}: any = this.props;

        return (
            <TableCell
                component="div"
                className={clsx(classes.tableCell, classes.flexContainer, {
                    [classes.noClick]: onRowClick == null,
                })}
                variant="body"
                style={{height: rowHeight, width: columns[columnIndex] ? columns[columnIndex].width : 0}}
                align={(columnIndex != null && columns[columnIndex] && columns[columnIndex].numeric) || false ? 'right' : 'left'}
            >
                {
                    columnIndex === 0 && hasSelection ? (
                        <Checkbox
                            checked={isSelected}
                            onClick={event => event.stopPropagation()}
                            onChange={event => this.handleCheck(event, rowData.id)}
                        />
                    ) : (

                        columnIndex === totalIndex - 1 && hasActions ? (
                            <div className={'flex'}>
                                <Tooltip title="Delete">
                                    <IconButton aria-label="Delete">
                                        <DeleteIcon onClick={(e) => onDeleteItem(rowData, e)}/>
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title="Edit">
                                    <IconButton aria-label="Edit">
                                        <EditIcon onClick={() => onEditItem(rowData)}/>
                                    </IconButton>
                                </Tooltip>
                            </div>
                        ) : this.renderCellValue(dataKey, rowData, columns[columnIndex])
                    )
                }
            </TableCell>
        );
    };

    headerRenderer = (opt: any) => {
        const {label, columnIndex, dataKey} = opt;
        const {headerHeight, columns, classes}: any = this.props;


        return (
            <TableCell
                component="div"
                className={clsx(classes.tableCell, classes.flexContainer, classes.noClick)}
                variant="head"
                style={{height: headerHeight}}
                align={columns[columnIndex] && columns[columnIndex].numeric || false ? 'right' : 'left'}
            >
                <b style={{fontWeight: 900}}>{label}</b>

            </TableCell>
        );
    };


    render() {
        const {classes, columns, hasActions, hasSelection, ...tableProps}: any = this.props;
        const {rows}: any = this.props;
        const _columns: any = [];
        if (hasSelection) {
            _columns.push(
                {
                    minWidth: 80,
                    label: '',
                    dataKey: '__checkbox',
                },
            );
        }
        columns.forEach((el: any) => {
            el.minWidth = el.width || 320;
            if (el.dataKey && el.dataKey.toLowerCase().match('name') || ['keywords'].indexOf(el.dataKey.toLowerCase()) > -1) {
                el.minWidth = 220;
            }

        });
        _columns.push(...columns);
        if (hasActions) {
            _columns.push(
                {
                    minWidth: 320,
                    label: 'Actions',
                    dataKey: '__actions',
                },
            );
        }
        _columns.forEach((el: any) => {

            el.maxWidth = 320;

        });

        return (
            <React.Fragment>
               {/* <table id={'datatable'}>
                    <tbody>
                    <tr>
                        {
                            columns.map((el: any) => {
                                return (
                                    <td key={el.label}>{el.label}</td>
                                )
                            })
                        }
                    </tr>
                    {
                        rows.map((row: any) => {
                            return (
                                <tr key={row.id}>
                                    {
                                        columns.map((column: any) => {
                                            return <td key={`${row.id}-${column.title}`}>{row[column.dataKey]}</td>
                                        })
                                    }
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>*/}
                <AutoSizer>
                    {({height, width}: any) => (
                        <Table height={height} width={width}   {...tableProps} rowClassName={this.getRowClassName}>
                            {_columns.map(({dataKey, ...other}: any, index: any) => {
                                return (
                                    <Column
                                        key={dataKey}

                                        headerRenderer={(headerProps: any) =>
                                            this.headerRenderer({
                                                ...headerProps,
                                                columnIndex: index,
                                                dataKey,
                                                totalIndex: _columns.length,
                                            })
                                        }
                                        className={classes.flexContainer}
                                        cellRenderer={(opt: any) => this.cellRenderer({
                                            ...opt,
                                            totalIndex: _columns.length,
                                        })}
                                        dataKey={dataKey}
                                        {...other}
                                    />
                                );
                            })}
                        </Table>
                    )}
                </AutoSizer>
            </React.Fragment>
        );
    }
}


const VirtualizedTable = withStyles(styles)(MuiVirtualizedTable);

export default function ReactVirtualizedTable({hasSelection, cellheight, onRowClick, hasActions, items, height, columns, selected, onSelectAllClick, onDeleteItem, onEditItem}: any) {
    function getRowHeight({index}: any): number {
        if (cellheight) return cellheight;
        const row = items[index];
        for (let i = 0; i < columns.length; i++) {
            const cellVal = row[columns[i].dataKey];
            if (cellVal && cellVal.toString().length > 32) {
                return 64;
            } else if (cellVal && cellVal.toString().length > 44) {
                return 84;
            }
        }
        return 44;
    }

    return (
        <Paper style={{height: height || window.innerHeight - 80, width: 'calc(100vw - 370px)'}}>
            <VirtualizedTable
                onRowsRendered={(e: any) => console.log(e)}
                hasActions={hasActions}
                hasSelection={hasSelection}
                onDeleteItem={onDeleteItem}
                onEditItem={onEditItem}
                selected={selected}
                onSelectAllClick={onSelectAllClick}
                rowCount={items.length}
                rowGetter={({index}: any) => items[index]}
                onRowClick={onRowClick}
                rowHeightGetter={getRowHeight}
                maxWidth={320}
                minWidth={320}
                rowHeight={44}
                rows={items}
                columns={columns}
            />
        </Paper>
    );
}
