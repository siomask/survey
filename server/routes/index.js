const path = require("path");
const router = require("express").Router();

const {auth} = require("../middleware/authenticate");

router.use("/api", auth, require("./api"));

router.use("/auth", require("./auth"));
// router.use("/test", require("./test"));

module.exports = router;
