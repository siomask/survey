const router = require('express').Router();
const {OBJECT_TABLE_NAME} = require('../../../../models');

const {checkItem, pagination, editItem, deleteItem, createItem} = require('../../extra');
const MODEL_NAME = OBJECT_TABLE_NAME.CATEGORY;
const TABLE_NAME = MODEL_NAME;

async function checkIten(req, res, next) {
    checkItem(MODEL_NAME, req, res, next);
}

router.put('/:id', checkIten, async function (req, res) {
    editItem({TABLE_NAME}, req, res);
});

router.delete('/:id', checkIten, async function (req, res) {
    deleteItem({TABLE_NAME}, req, res);
});

router.post("", async function (req, res) {
    createItem({TABLE_NAME}, req, res);

});

router.get('', function (req, res) {
    pagination({TABLE_NAME}, req, res);
});


module.exports = router;
