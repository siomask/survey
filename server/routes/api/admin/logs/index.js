
const router = require('express').Router();
const {models} = require('../../../../models');

const {SERVER_CODES, pagination} = require('../../extra');
const MODEL_NAME = 'Logs';
const TABLE_NAME = MODEL_NAME;

router.get('', function (req, res) {
    pagination({TABLE_NAME}, req, res,{include: [models.Users]});
});

module.exports = router;
