const router = require("express").Router();

router.all("/", function (req, res) {
    res.json({
        status: true,
        message: "Info about api",
        user: req.user
    });
});

router.use("/logs", require("./logs"));

router.use("/user", require("./user"));
router.use("/category", require("./category"));

module.exports = router;
