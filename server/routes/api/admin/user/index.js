const router = require('express').Router();
const {models, OBJECT_TABLE_NAME} = require('../../../../models');

const {SERVER_CODES, checkItem, pagination} = require('../../extra');
const MODEL_NAME = OBJECT_TABLE_NAME.USERS;
const TABLE_NAME = MODEL_NAME;

async function checkIten(req, res, next) {
    checkItem(MODEL_NAME, req, res, next);
}

router.put('/:id', checkIten, async function (req, res) {
    try {
        const updates = {
            ...req.body,
        };
        updates[OBJECT_TABLE_NAME.USER_PROJECTS] = null;
        delete updates[OBJECT_TABLE_NAME.USER_PROJECTS];
        if (req.body.role) {
            req.body.role = parseInt(req.body.role);
            if (
                req.user.role === 1 && req[MODEL_NAME].id === req.user.id && req.body.role > 1 ||
                req[MODEL_NAME].id === req.user.id && req.body.role !== req[MODEL_NAME].role
            ) {
                throw 'Not able to change the access';
            }
        }
        // if (typeof updates.password !== 'undefined' && updates.password) updates.password = models.Users.generateHash(updates.password);
        const keys = ['firstName', 'secondName', 'email', 'role'];
        const _u = [];
        for (let i = 0; i < keys.length; i++) {
            if (req[MODEL_NAME][keys[i]] !== updates[keys[i]]) {
                _u.push({
                    key: keys[i],
                    from: req[MODEL_NAME][keys[i]],
                    to: updates[keys[i]]
                });
            }
        }
        const item = req[MODEL_NAME];
        await req[MODEL_NAME].update(updates);
        const projects = await req[MODEL_NAME][`get${OBJECT_TABLE_NAME.USER_PROJECTS}`]();
        /*
        * delete projects
        * */
        for (let i = 0; i < projects.length; i++) {
            let hasItem = false;
            let list = [];
            for (let j = 0; j < req.body[OBJECT_TABLE_NAME.USER_PROJECTS].length; j++) {
                if (req.body[OBJECT_TABLE_NAME.USER_PROJECTS][j].id === projects[i].id) {
                    hasItem = true;
                }
            }
            if (!hasItem) {
                await item[`remove${OBJECT_TABLE_NAME.USER_PROJECTS}`](projects[i].id, {force: true});
                //await projects[i].destroy({truncate: true, cascade: false});
            }
        }
        /*
       *
       * assign projects
       * */
        for (let i = 0; i < req.body[OBJECT_TABLE_NAME.USER_PROJECTS].length; i++) {
            let hasItem = false;
            for (let j = 0; j < projects.length; j++) {
                if (req.body[OBJECT_TABLE_NAME.USER_PROJECTS][i].id === projects[j].id) {
                    hasItem = true;
                }
            }
            if (!hasItem) {
                await item[`add${OBJECT_TABLE_NAME.USER_PROJECTS}`](req.body[OBJECT_TABLE_NAME.USER_PROJECTS][i].id);
            }
        }

        Object.assign(req[MODEL_NAME], updates);

        models.Logs.create({
            action: `updated record ${TABLE_NAME}`,
            ip: req.clientIp.ip(),
            userId: req.userId,
            description: JSON.stringify(_u)
        });
        req[MODEL_NAME].projects = await req[MODEL_NAME][`get${OBJECT_TABLE_NAME.USER_PROJECTS}`]();
        res.json({
            status: true,
            message: `${MODEL_NAME} was updated`,
            data: req[MODEL_NAME],
        });
    } catch (error) {
        console.log(error);
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.EDIT_ITEM_ERROR,
            message: `${MODEL_NAME} was not updated`,
            data: req.item,
        });
    }
});

router.delete('/:id', checkIten, async function (req, res) {
    try {
        if (req.user.id === req[MODEL_NAME].id) throw 'Can`t delete self!';
        await req[MODEL_NAME].destroy();
        res.json({
            status: true,
            message: `Item ${MODEL_NAME} was deleted`,
            data: req[MODEL_NAME],
        });
        models.Logs.create({
            action: `updated record ${TABLE_NAME}`,
            ip: req.clientIp.ip(),
            userId: req.userId,
            description: `record ${req.item.id} was deleted`
        });
    } catch (error) {
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.DELETE_ITEM_ERROR,
            message: `${MODEL_NAME} was not deleted`,
            data: req.item,
        });
    }
});

router.post("", async function (req, res) {
    try {
        const updates = {
            ...req.body,
        };
        updates[OBJECT_TABLE_NAME.USER_PROJECTS] = null;
        delete updates[OBJECT_TABLE_NAME.USER_PROJECTS];
        const link = req.protocol + '://' + req.get('host');
        const item = await models[MODEL_NAME].register({
            ...updates,
            userId: req.user.id
        }, {link});
        if (req.body[OBJECT_TABLE_NAME.USER_PROJECTS]) {
            for (let i = 0; i < req.body[OBJECT_TABLE_NAME.USER_PROJECTS].length; i++) {
                await item[`add${OBJECT_TABLE_NAME.USER_PROJECTS}`](req.body[OBJECT_TABLE_NAME.USER_PROJECTS][i].id);
            }
        }
        models.Logs.create({
            action: `create record ${TABLE_NAME}`,
            ip: req.clientIp.ip(),
            userId: req.userId,
            description: `${item.id} was created`
        });
        item.projects = await item[`get${OBJECT_TABLE_NAME.USER_PROJECTS}`]();
        res.json(item);
    } catch (error) {
        console.log(error);
        res.status(400).json({
            statusCODE: SERVER_CODES.CREATE_ERROR,
            error,
            message: `Not able to create ${MODEL_NAME}`
        });
    }

});

router.get('', function (req, res) {
    pagination({TABLE_NAME}, req, res);
});

router.get("/about", async function (req, res) {
    res.json({
        status: true,
        data: req.user,
    });
});

module.exports = router;
