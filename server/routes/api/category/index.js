const router = require('express').Router();
const {OBJECT_TABLE_NAME} = require('../../../models');

const {pagination} = require('../extra');
const MODEL_NAME = OBJECT_TABLE_NAME.CATEGORY;
const TABLE_NAME = MODEL_NAME;

router.get('', function (req, res) {
    pagination({TABLE_NAME}, req, res,{paranoid: !req.query.paranoid});
});

module.exports = router;
