const SERVER_CODES = {
    BAD_STATUS: 400,
    FETCH_ERROR: 1,
    CREATE_ERROR: 2,
    GET_ITEM_ERROR: 3,
    EDIT_ITEM_ERROR: 4,
    DELETE_ITEM_ERROR: 5,
};
const {models, Op, OBJECT_TABLE_NAME} = require('../../models');
const SETTINGS = require('../../models/config');
const fs = require('fs');
const CONFIG = require('../../config');
const multer = require('multer');
const exif = require('jpeg-exif');
const moment = require('moment');
// SET STORAGE
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '../resources/')
    },
    filename: function (req, file, cb) {
        file._fieldname = Date.now() + "-" + file.originalname;
        cb(null, file._fieldname)
    }
});

// var multer  = require('multer');
// var upload = multer({ dest: 'uploads/' })


function readExifData(filePath) {
    return new Promise((resolve, reject) => {
        exif.parse(filePath, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log(data);
                resolve(data);
            }
        });
    })
}

const upload = multer({storage: storage});

async function checkItem(modelName, req, res, next, paramId = 'id') {
    try {
        const item = await models[modelName].findByPk(req.params[paramId]);
        if (!item) throw `No ${modelName} available!`;
        req[modelName] = req.item = item;
        next();
    } catch (error) {
        res.status(400).json({
            statusCODE: SERVER_CODES.GET_ITEM_ERROR,
            error,
            message: error.message || error
        });
    }
}

function pagination({TABLE_NAME}, req, res, opt) {
    let options = {
        include: []
    };
    let filters = {};
    let order = [
        ['createdAt', 'DESC']
    ];
    let uploadsKEY = '';
    let where = {};
    if ([OBJECT_TABLE_NAME.USERS, OBJECT_TABLE_NAME.LOGS, OBJECT_TABLE_NAME.CATEGORY].indexOf(TABLE_NAME) > -1) {

        if (TABLE_NAME === OBJECT_TABLE_NAME.USERS) {
            options.include.push({model: models.Projects, as: OBJECT_TABLE_NAME.USER_PROJECTS});
        }
    } else {
        where = {
            projectId: req.query.projectsList ? JSON.parse(req.query.projectsList) : req.Projects.id,
        };
        if (req.Powerlines) {
            where = {
                powerLineId: req.query.powerlinesList ? JSON.parse(req.query.powerlinesList) : req.Powerlines.id,
            }
        }
    }
    try {
        if (req.query.filter) {
            const filter = JSON.parse(req.query.filter);
            const isFullSearch = parseInt(req.query.isFullSearch) === 1;
            const FilterAct = isFullSearch ? Op.or : Op.and;
            filter.forEach((a) => {
                    if (!a.value) return;
                    if (!filters[FilterAct]) filters = {
                        [FilterAct]: []
                    };
                    if (OBJECT_TABLE_NAME.LOGS === TABLE_NAME) {
                        if (a.columnName === 'user') {
                            return opt.include = {
                                model: models.Users,
                                where: {
                                    email: {
                                        [Op.iLike]: `%${a.value.toString().toLowerCase()}%`
                                    }
                                }
                            }
                        }
                    }
                    if (a.columnName === 'id') {
                        const intValue = parseInt(a.value);
                        if (!isNaN(intValue)) {
                            filters[FilterAct].push(
                                {
                                    [a.columnName]: {
                                        [Op.eq]: parseInt(a.value)
                                    }
                                }
                            );
                        }
                    } else if (a.columnName === 'status') {
                        const intValue = parseInt(a.value);
                        if (!isNaN(intValue)) {
                            filters[FilterAct].push(
                                {
                                    [a.columnName]: {
                                        [Op.eq]: parseInt(a.value)
                                    }
                                }
                            );
                        }
                    } else if (a.columnName.match('At')) {
                        const range = (a.value);
                        if (range.start) {
                            filters[FilterAct].push(
                                {
                                    [a.columnName]: {
                                        [Op.gte]: range.start
                                    }
                                }
                            );
                        }
                        if (range.end) {
                            filters[FilterAct].push(
                                {
                                    [a.columnName]: {
                                        [Op.lte]: range.end
                                    }
                                }
                            );
                        }
                        if (isFullSearch) {
                            try {
                                const date = new Date(a.value.toString()).toISOString();
                                filters[FilterAct].push(
                                    {
                                        [a.columnName]: {
                                            [Op.eq]: date
                                        }
                                    }
                                );
                            } catch (e) {
                                console.log(e);
                            }

                        }
                    } else {
                        filters[FilterAct].push(
                            {
                                [a.columnName]: {
                                    [Op.iLike]: `%${a.value.toString().toLowerCase()}%`
                                }
                            }
                        );
                        // filters[a.columnName] = {
                        //     [Op.iLike]: `%${a.value.toString().toLowerCase()}%`
                        // }
                    }
                }
            );
        }
        if (req.query.sort) {
            try {
                const sort = JSON.parse(req.query.sort)[0];
                if (sort) order = [
                    [
                        sort.selector,
                        sort.desc ? 'DESC' : "ASC",

                    ]
                ];
            } catch (e) {
                console.log(e);
            }
        }

        switch (TABLE_NAME) {
            case OBJECT_TABLE_NAME.PARCELS: {
                uploadsKEY = OBJECT_TABLE_NAME.PARCEL_UPLOADS;
                break;
            }
            case OBJECT_TABLE_NAME.POLES: {
                uploadsKEY = OBJECT_TABLE_NAME.POLE_UPLOADS;
                break;
            }
            case OBJECT_TABLE_NAME.STATIONS: {
                uploadsKEY = OBJECT_TABLE_NAME.STATION_UPLOADS;
                break;
            }
            case OBJECT_TABLE_NAME.SEGMENTS: {
                uploadsKEY = OBJECT_TABLE_NAME.SEGMENT_UPLOADS;
                break;
            }
            case OBJECT_TABLE_NAME.POI: {
                uploadsKEY = OBJECT_TABLE_NAME.POI_UPLOADS;
                break;
            }
        }
        if (uploadsKEY) options.include.push({model: models.Uploads, as: uploadsKEY});
        if (opt) {
            options = {
                ...options,
                ...opt
            }
        }
    } catch (e) {
        console.log(e);
    }


    models[TABLE_NAME].findAndCountAll({
        where: {
            ...where,
            ...filters,
        },
        // order: 'createdAt DESC',
        limit: req.query.limit || 10,
        offset: req.query.offset || 0,
        distinct: true,
        order,
        ...options
    }).then((result) => {
        if (uploadsKEY) {
            result.rows.forEach((el) => {
                el.uploads = el[uploadsKEY];
            });
        }
        if(TABLE_NAME ===OBJECT_TABLE_NAME.SEGMENTS){
            console.log(result.rows.length);
        }
        res.json(result);
    }).catch((error) => {
        console.log(error);
        res.status(400).json({
            statusCODE: SERVER_CODES.FETCH_ERROR,
            error,
            message: `Not table to fetch ${TABLE_NAME}`
        })
    })
}

async function deleteItem({TABLE_NAME}, req, res) {
    try {
        await req.item.destroy();
        models.Logs.create({
            action: `delete record ${TABLE_NAME}`,
            ip: req.clientIp.ip(),
            userId: req.userId,
            description: `record ${req.item.id} was removed`
        });
        res.json({
            status: true,
            message: `Item ${TABLE_NAME} was deleted`,
            data: req.item,
        });
    } catch (error) {
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.DELETE_ITEM_ERROR,
            message: `Item ${TABLE_NAME} was not deleted`,
            data: req.item,
        });
    }
}

async function editItem({TABLE_NAME}, req, res) {
    try {
        if (moment(req.body.updatedAt).isBefore(moment(req.item.updatedAt))) {
            return res.json({
                status: true,
                message: 'Item was not updated, someone else already made changes',
                data: req.item,
            });
        }
        const updates = {};
        const updates_vale = [];
        const updateFields = [
            'status',
            'title',
            'description',
            'comment',
        ];
        switch (TABLE_NAME) {
            case OBJECT_TABLE_NAME.SEGMENTS: {
                updateFields.push(
                    'vegetation_status',
                    'distance_lateral',
                    'distance_bottom',
                    'notes',
                );
                if (
                    [
                        SETTINGS.SEGMENT_STATUS.SHUT_OFF,
                    ].indexOf(req.body.status) > -1
                ) {
                    updateFields.push(
                        'shutdown_time',
                        'track',
                    );
                }
                if (
                    [
                        SETTINGS.SEGMENT_STATUS.SERVICE,
                        SETTINGS.SEGMENT_STATUS.no_permission,
                        SETTINGS.SEGMENT_STATUS.URGENT,
                        SETTINGS.SEGMENT_STATUS.VEGETATED,
                        SETTINGS.SEGMENT_STATUS.SHUT_OFF,
                    ].indexOf(req.body.status) > -1
                ) {
                    updateFields.push(
                        'operation_type',
                        'time_of_operation',
                    );
                }

                if (
                    [
                        SETTINGS.SEGMENT_STATUS.SERVICE,
                    ].indexOf(req.body.status) > -1
                ) {
                    updateFields.push(
                        'time_for_next_entry',
                    );
                }
                if (
                    [
                        SETTINGS.SEGMENT_STATUS.no_permission,
                    ].indexOf(req.body.status) > -1
                ) {
                    updateFields.push(
                        'parcel_number_for_permit',
                    );
                }

                break;
            }
        }

        if (req.user.role === models.Users.__SETTINGS.ROLE.SUPER_USER) {
            const fieldElems = [];
            switch (TABLE_NAME) {
                case OBJECT_TABLE_NAME.PARCELS: {
                    fieldElems.push(
                        'ownership',
                        'numer',
                        'wojewodztw',
                        'gmina'
                    );
                    break;
                }
                case OBJECT_TABLE_NAME.POLES: {
                    fieldElems.push(
                        'num_slup'
                    );
                    break;
                }
                case OBJECT_TABLE_NAME.SEGMENTS: {
                    fieldElems.push(
                        'nazwa_linii',
                        'NAZWA_TAB',
                        'nazwa_ciagu_id',
                        'przeslo',
                    );
                    break;
                }
                case OBJECT_TABLE_NAME.STATIONS: {
                    fieldElems.push(
                        'num_eksp_s',
                        'nazw_stac'
                    );
                    break;
                }
            }
            updateFields.push(
                ...fieldElems
            );
        }

        /*-------UPDATE uploads------*/
        const tableName = TABLE_NAME.substr(0, TABLE_NAME.length - 1);
        const methodUpload = `add${tableName}${OBJECT_TABLE_NAME.UPLOADS}`;
        const getMethodUpload = `get${tableName}${OBJECT_TABLE_NAME.UPLOADS}`;
        if (Array.isArray(req.body.uploads)) {
            const uploads = await req.item[getMethodUpload]();

            const tableRelation = `${tableName}_${OBJECT_TABLE_NAME.UPLOADS}`.toLowerCase();
            // console.log(uploads[0].parcel_uploads.uploadId);
            const exist = [];
            for (let i = 0; i < req.body.uploads.length; i++) {
                const el = req.body.uploads[i];
                let hasAlready = false;
                for (let di = 0; di < uploads.length; di++) {
                    if (uploads[di][tableRelation].uploadId === el.id) {
                        exist.push(el.id);
                        hasAlready = true;
                        break;//ALREADY exist
                    }
                }
                if (!hasAlready) await req.item[methodUpload](el.id);

            }
            const removed = [];
            const filesToRemove = uploads.filter((el) => exist.indexOf(el[tableRelation].uploadId) < 0);
            for (let i = 0; i < filesToRemove.length; i++) {
                const filePath = `${CONFIG.DIR.RESOURCES}/resources/${filesToRemove[i].path}`;
                fs.unlink(filePath, function (err) {
                    if (err) {
                        console.log(err);
                        console.log(`---File ${filePath} was not removed`);
                    } else {
                        console.log(`---File ${filePath} was removed`);
                    }
                });
                removed.push(filePath);
                await filesToRemove[i].destroy({force: true});
            }
            if (removed.length) {
                updates_vale.push(
                    {
                        key: 'upload_removed',
                        values: removed
                    }
                );
            }
            const uploadsIds = uploads.map((el) => el[tableRelation].uploadId);
            const newUploads = req.body.uploads.filter((el) => uploadsIds.indexOf(el.id) < 0).map((el) => el.path);
            if (newUploads.length) {
                updates_vale.push(
                    {
                        key: 'upload',
                        values: newUploads
                    }
                );
            }
        }
        /*-------UPDATE uploads------*/

        updateFields.forEach((el) => {
            const value = req.body[el];
            if (
                req.body.hasOwnProperty(el) &&
                value !== req.item[el]
            ) {
                updates[el] = value;
                updates_vale.push(
                    {
                        key: el,
                        from: req.item[el],
                        to: value
                    }
                );
            }
        });

        await req.item.update(updates);
        if (req.item[getMethodUpload]) req.item.uploads = await req.item[getMethodUpload]();
        models.Logs.create({
            action: `update record ${TABLE_NAME}`,
            ip: req.clientIp.ip(),
            userId: req.userId,
            description: JSON.stringify(updates_vale).substr(0, 1500)
        });
        res.json({
            status: true,
            message: 'Item was updated',
            data: req.item,
        });
    } catch (error) {
        console.log(error);
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.EDIT_ITEM_ERROR,
            message: 'Item was not updated',
            data: req.item,
        });
    }
}

async function createItem({TABLE_NAME}, req, res) {
    try {
        const body = {
            ...req.body,
            userId: req.user.id
        };
        if (req.Projects) {
            body.projectId = req.Projects.id;
        }
        const item = await models[TABLE_NAME].create(body);

        try {
            const tableName = TABLE_NAME.substr(0, TABLE_NAME.length - 1);
            const methodUpload = `add${tableName}${OBJECT_TABLE_NAME.UPLOADS}`;
            const getMethodUpload = `get${tableName}${OBJECT_TABLE_NAME.UPLOADS}`;
            if (item[methodUpload] && Array.isArray(req.body.uploads)) {

                for (let i = 0; i < req.body.uploads.length; i++) {
                    const el = req.body.uploads[i];
                    await item[methodUpload](el.id);
                }
            }
            if (item[getMethodUpload]) item.uploads = await item[getMethodUpload]();
        } catch (e) {
            console.log(e);
        }


        models.Logs.create({
            action: `create record ${TABLE_NAME}`,
            ip: req.clientIp.ip(),
            userId: req.userId,
            description: `record ${item.id} was created`
        });
        res.json(item);
    } catch (error) {
        console.log(error);
        res.status(400).json({
            statusCODE: SERVER_CODES.CREATE_ERROR,
            error,
            message: `Not able to create ${TABLE_NAME}`
        });
    }
}

module.exports = {
    upload,
    SERVER_CODES,
    readExifData,
    editItem,
    createItem,
    deleteItem,
    pagination,
    checkItem
};
