const router = require('express').Router();
const {models} = require('../../../models');
const CONFIG = require('../../../config');

const {SERVER_CODES, checkItem, pagination, upload, readExifData} = require('../extra');
const TABLE_NAME = 'Uploads';

router.get('/:id', checkItem, function (req, res) {
    res.json({
        status: true,
        data: req.item,
    });
});


router.delete('/:id', checkItem, async function (req, res) {
    try {
        await req.item.destroy();
        res.json({
            status: true,
            message: 'Item was deleted',
            data: req.item,
        });
    } catch (error) {
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.DELETE_ITEM_ERROR,
            message: 'Item was not deleted',
            data: req.item,
        });
    }
});

router.post("", upload.any(), async function (req, res) {
    try {
        const files = req.files || req.file;
        if (!files || files.length === 0) {
            throw 'No files provided';
        }
        let metada = '';
        try {
            metada = await readExifData(`${CONFIG.DIR.RESOURCES}/${files[0]._fieldname}`);
            if(metada) {
                metada = JSON.stringify({
                    DateTime: metada.DateTime,
                    GPSInfo: metada.GPSInfo
                });
            }
        } catch (e) {
            console.log(e);
        }
        const item = await models[TABLE_NAME].create({
            path: files[0]._fieldname,
            metada,
            userId: req.user.id
        });
        res.json(item);
    } catch (error) {
        console.log(error);
        res.status(400).json({
            statusCODE: SERVER_CODES.CREATE_ERROR,
            error,
            message: `Not able to create ${TABLE_NAME}`
        });
    }

});

router.get("", async function (req, res) {
    pagination({TABLE_NAME}, req, res);
});

module.exports = router;

