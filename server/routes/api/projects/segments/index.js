const router = require('express').Router();
const {models, OBJECT_TABLE_NAME} = require('../../../../models');

const {SERVER_CODES, checkItem, pagination, editItem, deleteItem} = require('../../extra');
const MAP_ELEMENT = OBJECT_TABLE_NAME.SEGMENTS;
const TABLE_NAME = MAP_ELEMENT;

async function checkIten(req, res, next) {
    checkItem(MAP_ELEMENT, req, res, next);
}

router.get('/:id', checkIten, function (req, res) {
    res.json({
        status: true,
        data: req.item,
    });

});

router.put('/:id', checkIten, async function (req, res) {
    editItem({TABLE_NAME}, req, res);
});

router.delete('/:id', checkIten, async function (req, res) {
    deleteItem({TABLE_NAME}, req, res);
});

router.post("", async function (req, res) {
    try {
        // if (!req.body.powerLineid) throw 'Power Line is required';
        if (!req.body.poles) throw 'Poles are required';
        const item = await models.Segments.create({
            ...req.body,
            userId: req.user.id
        });
        // await item.addPoleSegments(req.body.poles);
        // await item.addSegmentPowerline([req.body.powerLineid]);
        item.dataValues.poles = await item.getPoleSegments().map((el) => el.id);
        res.json(item);
    } catch (error) {
        console.log(error);
        res.status(400).json({
            statusCODE: SERVER_CODES.CREATE_ERROR,
            error,
            message: 'Not able to create Segment'
        });
    }

});

router.get("", async function (req, res) {
    pagination({TABLE_NAME}, req, res, {paranoid: !req.query.paranoid});
});

module.exports = router;
