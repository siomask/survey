const router = require('express').Router();
const {models, OBJECT_TABLE_NAME} = require('../../../../models');

const {SERVER_CODES, checkItem, pagination, editItem, deleteItem,createItem} = require('../../extra');
const TABLE_NAME = OBJECT_TABLE_NAME.POI;

async function checkIten(req, res, next) {
    await checkItem(TABLE_NAME, req, res, next);
}

router.get('/:id', checkIten, function (req, res) {
    res.json({
        status: true,
        data: req.item,
    });
});

router.put('/:id', checkIten, async function (req, res) {
    editItem({TABLE_NAME}, req, res);
});

router.delete('/:id', checkIten, async function (req, res) {
    deleteItem({TABLE_NAME}, req, res);
});

router.post("", async function (req, res) {
    createItem({TABLE_NAME}, req, res);
});

router.get("", async function (req, res) {
    pagination({TABLE_NAME}, req, res, {paranoid: !req.query.paranoid});
});

module.exports = router;
