const router = require('express').Router();
const {models, OBJECT_TABLE_NAME} = require('../../../../models');

const {SERVER_CODES, checkItem, pagination, editItem, deleteItem} = require('../../extra');
const TABLE_NAME = OBJECT_TABLE_NAME.STATIONS;

async function checkIten(req, res, next) {
    checkItem(TABLE_NAME, req, res, next);
}

router.get('/:id', checkIten, function (req, res) {
    res.json({
        status: true,
        data: req.item,
    });

});

router.put('/:id', checkIten, async function (req, res) {
    editItem({TABLE_NAME}, req, res);
});

router.delete('/:id', checkIten, async function (req, res) {
    deleteItem({TABLE_NAME}, req, res);
});

router.post("", async function (req, res) {
    try {
        const item = await models[TABLE_NAME].create({
            ...req.body,
            userId: req.user.id,
            projectId: req.Projects.id
        });
        res.json(item);
    } catch (error) {
        res.status(400).json({
            statusCODE: SERVER_CODES.CREATE_ERROR,
            error,
            message: `Not able to create ${TABLE_NAME}`
        });
    }

});

router.get("", async function (req, res) {
    pagination({TABLE_NAME}, req, res, {paranoid: !req.query.paranoid});
});

module.exports = router;
