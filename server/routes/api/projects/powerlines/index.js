const router = require('express').Router();
const {models} = require('../../../../models');

const {SERVER_CODES, checkItem, pagination} = require('../../extra');
const MAP_ELEMENT = 'Powerlines';
const TABLE_NAME = MAP_ELEMENT;
const MAP_SCHEMA = MAP_ELEMENT;

async function checkIten(req, res, next) {
    checkItem(MAP_SCHEMA, req, res, next);
}

async function checkItemId(req, res, next) {
    checkItem(MAP_SCHEMA, req, res, next, 'powerlineId');
}

router.get("", async function (req, res) {
    if(req.query.projectsList) {
        pagination({TABLE_NAME}, req, res, {paranoid: !req.query.paranoid});
    } else {
        try {
            const list = await req.Projects.getProjectPowerline();
            res.json({
                rows: list
            });
        } catch (e) {
            console.log(e);
        }
    }
});

router.use("/:powerlineId/parcels", checkItemId, require("./parcels"));
router.use("/:powerlineId/poles", checkItemId, require("./poles"));
router.use("/:powerlineId/segments", checkItemId, require("./segments"));
module.exports = router;
