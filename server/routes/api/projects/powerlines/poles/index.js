const router = require('express').Router();
const {models, OBJECT_TABLE_NAME} = require('../../../../../models');
const {SERVER_CODES, checkItem, pagination} = require('../../../extra');
const TABLE_NAME = OBJECT_TABLE_NAME.POLES;
router.get("", async function (req, res) {
    pagination({TABLE_NAME}, req, res, {paranoid: !req.query.paranoid});
});

module.exports = router;
