const router = require('express').Router();
const {models, OBJECT_TABLE_NAME} = require('../../../models');

const {SERVER_CODES, checkItem} = require('../extra');
const MODEL_NAME = OBJECT_TABLE_NAME.USERS;
const TABLE_NAME = MODEL_NAME;

async function checkIten(req, res, next) {
    checkItem(MODEL_NAME, req, res, next);
}


router.put('/:id', checkIten, async function (req, res) {
    try {
        const updates = {
            ...req.body,
        };

        if (typeof updates.password !== 'undefined' && updates.password) {
            updates.password = models.Users.generateHash(updates.password);
            updates.status = models.Users.__SETTINGS.STATUS.APPROVED;
        }
        const keys = ['firstName', 'secondName', 'email', 'role', 'status'];
        const _u = [];
        for (let i = 0; i < keys.length; i++) {
            if (req[MODEL_NAME][keys[i]] !== updates[keys[i]]) {
                _u.push({
                    key: keys[i],
                    from: req[MODEL_NAME][keys[i]],
                    to: updates[keys[i]]
                });
            }
        }
        await req[MODEL_NAME].update(updates);
        Object.assign(req[MODEL_NAME], updates);

        models.Logs.create({
            action: `updated record ${TABLE_NAME}`,
            ip: req.clientIp.ip(),
            userId: req.userId,
            description: JSON.stringify(_u)
        });
        res.json({
            status: true,
            message: `${MODEL_NAME} was updated`,
            data: req[MODEL_NAME],
        });
    } catch (error) {
        console.log(error);
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.EDIT_ITEM_ERROR,
            message: `${MODEL_NAME} was not updated`,
            data: req.item,
        });
    }
});
router.get("/about", async function (req, res) {
    res.json({
        status: true,
        data: req.user,
    });
});

module.exports = router;
