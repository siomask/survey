const router = require("express").Router();
const request = require('request');
const {models} = require('../../models');

const {admin} = require("../../middleware/authenticate");
router.all("/", function (req, res) {
    res.json({
        status: true,
        message: "Info about api",
        user: req.user
    });
});

router.use("/projects", require("./projects"));
router.use("/admin", admin, require("./admin"));
router.use("/user", require("./user"));
router.use("/uploads", require("./uploads"));
router.use("/category", require("./category"));

router.use("/logout", async function (req, res) {
    try {
        const user = await models.Users.update({token: ''}, {where: {id: req.user.id}});
        return res.status(200).json({
            status: true,
            message: 'User was logged out!'
        });
    } catch (e) {
        return res.status(400).json({
            status: false,
            message: e.message || e
        });
    }
});
module.exports = router;
