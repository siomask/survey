const passport = require("passport");
const router = require("express").Router();
const async = require("async");
const jwt = require("jwt-simple");

var config = require("../../config");
var Mail = require("../../services/transporter.mail");

const {models, OBJECT_TABLE_NAME} = require("../../models");
const LOG = require("../../services/log");
const TABLE_NAME = OBJECT_TABLE_NAME.USERS;

router.all("/", function (req, res, next) {
    res.json({
        status: true,
        message: "Info about auth."
    });
});

router.post("/login", async function (req, res, next) {

    // req.session.views = 1;
    // console.log(req.body.email);
    var passportCtrl = function () {
        passport.authenticate("local", function (err, user, info) {
            if (err) {
                return res.status(400).json({
                    status: false,
                    err,
                    statusCODE: 1,
                    message: err.message || "Something bad."
                });
            }

            if (!user) {
                return res.status(400).json({
                    status: false,
                    statusCODE: 2,
                    message: info.message || "Email or password is incorrect."
                });
            }

            req.login(user, async function (err) {
                if (err) {
                    return res.status(400).json({
                        status: false,
                        statusCODE: 3,
                        message: err.message || "Something bad."
                    });
                }

                req.user = user;
                await LOG.saveIP(req, 'logged in');
                res.json({
                    status: true,
                    message: "Login success.",
                    access_token: user.token,
                    user: user
                });
            });
        })(req, res, next);
    };

    if ((req.body.email === config.superadmin.email) && (req.body.password === config.superadmin.password)) {
        try {
            const user = await models.Users.findByLogin(req.body.email);
            if (user) {

            } else {
                const link = req.protocol + '://' + req.get('host');
                await models.Users.register({
                    ...req.body,
                    role: models.Users.__SETTINGS.ROLE.SUPER_USER
                }, {
                    link
                });
            }
            passportCtrl();
        } catch (err) {
            console.log(err);
            return res.status(400).json({
                status: false,
                statusCODE: 4,
                err,
                message: err.message || "Something bad."
            });
        }
    } else {
        passportCtrl();
    }

});
router.post("/register", async function (req, res, next) {
    try {
        const user = await models.Users.register(req.body);
        return res.status(200).json({
            status: true,
            user,
            message: "User was registered."
        });
    } catch (err) {
        return res.status(400).json({
            status: false,
            statusCODE: 5,
            err,
            message: 'Email is already in use' || "Something bad."
        });
    }

});

router.post("/forgot-psw", async function (req, res) {
    try {
        const fullUrl = req.protocol + '://' + req.get('host') + "/reset-psw";// + req.originalUrl;
        const user = await models.Users.findByLogin(req.body.email);
        if (!user) throw 'No user found!';
        const tempForgotPswLink = jwt.encode({
            _id: user._id,
            email: user.email,
            created: Date.now()
        }, config.security.secret);
        const link = `${fullUrl}?resetpassword=${tempForgotPswLink}`;
        await Mail.send({
            reciever: user.email,
            title: 'Reset password',
            action: 'reset password',
            name: user.firstName,
            link
        });
        await models.Users.update({tempForgotPswLink}, {where: {id: user.id}});
        models.Logs.create({
            action: `updated record ${TABLE_NAME}`,
            ip: req.clientIp.ip(),
            userId: req.userId,
            description: `user ${user.id} was update password`
        });
        return res.status(200).json({
            status: true,
            tempForgotPswLink,
            message: 'Reset link was sent!',
            link
        });
    } catch (e) {
        return res.status(400).json({
            status: false,
            message: e.message || e
        });
    }
});
router.post("/reset-psw", async function (req, res) {
    try {
        const {password, tempForgotPswLink} = req.body;
        if (!password || !tempForgotPswLink) throw 'missing required data!';
        const user = await models.Users.findOne({where: {tempForgotPswLink: req.body.tempForgotPswLink}});
        if (!user) throw 'No user found!';
        if (user.tempForgotPswLink !== req.body.tempForgotPswLink) throw 'Reset token is invalid or expired!!!';


        await models.Users.update({
            password: models.Users.generateHash(password),
            tempForgotPswLink: ''
        }, {where: {id: user.id}});
        models.Logs.create({
            action: `updated record ${TABLE_NAME}`,
            ip: req.clientIp.ip(),
            userId: req.userId,
            description: `user ${user.id} was request to update password`
        });
        return res.status(200).json({
            status: true,
            message: 'Reset password success'
        });
    } catch (e) {
        return res.status(400).json({
            status: false,
            message: e.message || e
        });
    }
});

router.use("/logout", async function (req, res) {
    try {
        const user = await models.Users.update({token: ''}, {where: {id: req.user.id}});
        return res.status(200).json({
            status: true,
            message: 'User was logged out!'
        });
    } catch (e) {
        return res.status(200).json({
            status: false,
            message: e.message || e
        });
    }
});

module.exports = router;
