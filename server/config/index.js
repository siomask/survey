// process.env.NODE_ENV = 'production';selectedChild
const fs = require('fs');
var CONFIG = {
    USER_ROLE: {
        SUPER: 1,
    },
    port: process.env.PORT || 3009,
    mongoose: {
        uri: 'mongodb://localhost/nur',
    },
    DIR:{
        RESOURCES:'../resources'
    },
    security: {
        secret: 't45g3w45r34tw5ye454uhdgdf',
        expiresIn: '24h',
    },
    db: {
        postgress:  {
            DB_NAME: 'surveyapp',
            DB_USER: '',
            DB_SCHEMA: 'survey',
            DB_PSW: '',
            DB_HOST: '',
            port: '5432'
        }
    },
    superadmin: {
        email: 'simonpas323@gmail.com',
        password: '123',
    },
    help: {
        IP: (ip) => ip.replace('::ffff:', ''),
        deleteFolderRecursive: function (path, flag) {
            var _self = this;
            if (fs.existsSync(path)) {
                for (var u = 0, files = fs.readdirSync(path); u < files.length; u++) {
                    var file = files[u],
                        curPath = path + '/' + file;
                    if (fs.lstatSync(curPath).isDirectory()) { // recurse
                        _self.deleteFolderRecursive(curPath, true);
                    } else {
                        fs.unlinkSync(curPath);
                    }
                }
                if (flag) fs.rmdirSync(path);
            }
        },
        makeid: function (length = 32) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        },
    },
};
CONFIG.randomString = function (l) {

    var length_ = l || 25,
        chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
    if (typeof length_ !== 'number') {
        length_ = Math.floor(Math.random() * chars.length_);
    }
    var str = '';
    for (var i = 0; i < length_; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str + Date.now().toString(32);
};
CONFIG.saveImage = function (image, avatar, done) {


    if (!image) {
        if (avatar) {
            var filePath = './resources' + avatar;
            fs.unlinkSync(filePath);
        }
        if (done) done(null, '');
        return;
    }

    if (avatar) {
        var filePath = './resources' + avatar;
        fs.unlinkSync(filePath);
    }

    var imageTypeRegularExpression = /\/(.*?)$/;

    var crypto = require('crypto');
    var seed = crypto.randomBytes(20);
    var uniqueSHA1String = crypto.createHash('sha1').update(seed).digest('hex');

    var imageBuffer = CONFIG.decodeBase64Image(image);
    var userUploadedFeedMessagesLocation = 'resources/uploads/img/projects/';

    var uniqueRandomImageName = 'image-' + uniqueSHA1String; //uniqueSHA1String;
    var imageTypeDetected = CONFIG.decodeBase64Image(image).type.match(imageTypeRegularExpression);
    var userUploadedImagePath = userUploadedFeedMessagesLocation + uniqueRandomImageName + '.' + imageTypeDetected[1];
    var clientPath = '/uploads/img/projects/' + uniqueRandomImageName + '.' + imageTypeDetected[1];

    fs.writeFile(userUploadedImagePath, imageBuffer.data, 'base64', function (err) {
        console.log('DEBUG - feed:message: Saved to disk image attached by user:', userUploadedImagePath);
        if (done) done(err, clientPath);
    });
};
CONFIG.decodeBase64Image = function (dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    var response = {};

    if (matches.length !== 3)
        return new Error('Invalid input string');

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
};
module.exports = CONFIG;
