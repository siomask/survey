const {
    models,
} = require('../models');
const CONFIG = require('../config');
const WebServiceClient = require('@maxmind/geoip2-node').WebServiceClient;
const client = new WebServiceClient('77238', 'RJJqf1K9BLAO');


function getIPINfo(req) {
    return new Promise((resolve, reject) => {
        // const requestIpAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        const IP = req.clientIp;

        client.country(IP).then((country) => {
            client.city(IP).then(city => {
                client.insights(IP).then(insights => {
                    resolve({
                        IP,
                        country,
                        city,
                        insights,
                    });
                }).catch((e) => {
                    reject(e);
                });
            }).catch((e) => {
                reject(e);
            });
        }).catch((e) => {
            reject(e);
        });
    });
}

const LOG = {
    saveIP: function (req, action) {
        return new Promise(async (resolve, reject) => {
            let info;
            let IP = req.clientIp.ip();
            IP = IP.replace('::', '');
            try {
                info = await getIPINfo(req);
                await models.Logs.create({
                    action: action,
                    ip: IP,
                    userId: req.userId,
                    description: `${info.country.country.isoCode},${info.city.city.names ? info.city.city.names.en : ''},${info.city.traits.domain},${info.city.traits.isp}`
                });

            } catch (error) {
                await models.Logs.create({
                    action,
                    ip: IP,
                    userId: req.user.id,
                    description: `localhost`
                });

            } finally {
                resolve();
            }
        });

    },
};
module.exports = LOG;
