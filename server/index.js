const path = require('path');
const http = require('http');
const morgan = require('morgan');
const express = require('express');
const passport = require('passport');
const session = require('express-session');
const busboy = require('connect-busboy');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const requestIp = require('request-ip');

/*
const fs = require('fs');
const https = require('https');

const proxy = require('express-http-proxy');
*/

String.prototype.ip=function(){return this.replace('::ffff:', '')};
const config = require('./config');
const routes = require('./routes');
const {sequelize, models} = require('./models');
const cors = require('cors');


const app = express();
app.use(cors());
app.use(requestIp.mw());
app.use(morgan('dev'));
app.use(busboy());
app.use(cookieParser());
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, '../dist')));
app.use(express.static(path.join(__dirname, '../build')));
app.use('/resources', express.static(path.join(__dirname, '../resources')));
app.use('/', routes);
app.use('*', function (req, res) {
    res.sendFile(path.join(__dirname, '../build/index.html'));
});
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

if (process.env.NODE_ENV === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);

        res.json({
            message: 'Bad request',
            error: 'bad',
        });
    });
}

app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    console.error(err);
    res.send({
        message: err.message,
        error: {},
    });
});

//const settings = {force: process.env.NODE_ENV === 'development'};//if force true will clear all tables and restore structure
const settings = {force: false};
sequelize.sync(settings).then(() => {
    const server = http.createServer(app);
    server.on('listening', function () {
        var addr = server.address();
        var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;

        console.log('Listening on ' + bind);
    });

    server.on('error', function (error) {
        if (error.syscall !== 'listen') {
            throw error;
        }

        var bind = '';//typeof config.port === 'string' ? 'Pipe ' + config.port : 'Port ' + config.port;

        console.log(error);
        switch (error.code) {
            case 'EACCES':
                console.error(bind + ' requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(bind + ' is already in use');
                process.exit(1);
                break;
            default:
                throw error;
        }
    });

    server.listen(config.port, function (er) {
        console.log('Express server listening on port ', er);
    });
}).catch((e) => {
    console.log(e);
});
require('./middleware/passport')(passport);
