const {PREFIX} = require('./config');
const PoleUploads = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const PoleUploads = sequelize.define(`pole_uploads`, {
        poleId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        uploadId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },
    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX

    });

    PoleUploads.associate = models => {
        models.Poles.belongsToMany(models.Uploads, {through: PoleUploads,as: 'PoleUploads', foreignKey: 'poleId',onDelete: 'CASCADE'});
        models.Uploads.belongsToMany(models.Poles, {through: PoleUploads,as: 'PoleUploads', foreignKey: 'uploadId',onDelete: 'CASCADE'});
    };
    PoleUploads.__SETTINGS = __SETTINGS;

    return PoleUploads;
}

module.exports = PoleUploads;
