const {PREFIX,STATUS} = require('./config');
const Powerlines = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS ,
    };
    const Powerlines = sequelize.define(`powerlines`, {

        title: DataTypes.STRING,
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.UNDEFINED
        },
        comment: DataTypes.STRING,

        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Powerlines.associate = models => {
        Powerlines.belongsTo(models.Users, {foreignKey: 'userId',onDelete: 'CASCADE'});
    };

    Powerlines.__SETTINGS = __SETTINGS;

    return Powerlines;
};

module.exports = Powerlines;
