const {PREFIX, STATUS, PARCEL_OWNERSHIP} = require('./config');
const parcels = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS,
        PARCEL_OWNERSHIP
    };
    const Parcels = sequelize.define(`parcels`, {

        uploads: {
            type: DataTypes.VIRTUAL,
        },
        comment: DataTypes.STRING,
        title: DataTypes.STRING,
        points: DataTypes.GEOMETRY,

        wojewodztw: DataTypes.STRING,
        gmina: DataTypes.STRING,
        description: DataTypes.STRING,
        ownership: {
            type: DataTypes.STRING,
            defaultValue: __SETTINGS.PARCEL_OWNERSHIP.OTHER
        },
        numer: DataTypes.STRING,

        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.UNDEFINED
        },
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        powerLineId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },


    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Parcels.associate = models => {
        Parcels.belongsTo(models.Users, {foreignKey: 'userId',onDelete: 'CASCADE'});
        Parcels.belongsTo(models.Projects, {foreignKey: 'projectId',onDelete: 'CASCADE'});
        Parcels.belongsTo(models.Powerlines, {foreignKey: 'powerLineId',onDelete: 'CASCADE'});
    };

    Parcels.__SETTINGS = __SETTINGS;

    return Parcels;
}

module.exports = parcels;
