const {PREFIX, OBJECT_TABLE_NAME} = require('./config');
const ParcelUploads = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const ParcelUploads = sequelize.define(`parcel_uploads`, {
        parcelId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        uploadId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },
    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX

    });

    ParcelUploads.associate = models => {
        models.Parcels.belongsToMany(models.Uploads, {
            through: ParcelUploads,
            as: OBJECT_TABLE_NAME.PARCEL_UPLOADS,
            foreignKey: 'parcelId',onDelete: 'CASCADE'
        });
        models.Uploads.belongsToMany(models.Parcels, {
            through: ParcelUploads,
            as: OBJECT_TABLE_NAME.PARCEL_UPLOADS,
            foreignKey: 'uploadId',onDelete: 'CASCADE'
        });
    };
    ParcelUploads.__SETTINGS = __SETTINGS;

    return ParcelUploads;
}

module.exports = ParcelUploads;
