const {PREFIX, STATUS} = require('./config');
const poi = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS,
    };
    const Poi = sequelize.define(`poi`, {
        uploads: {
            type: DataTypes.VIRTUAL,
        },
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        points: DataTypes.GEOMETRY,
        comment: DataTypes.STRING,

        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.UNDEFINED
        },
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        categoryId: {
            type: DataTypes.INTEGER
        },
    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Poi.associate = models => {
        Poi.belongsTo(models.Users, {foreignKey: 'userId',onDelete: 'CASCADE'});
        Poi.belongsTo(models.Projects, {foreignKey: 'projectId',onDelete: 'CASCADE'});
        Poi.belongsTo(models.Category, {foreignKey: 'categoryId',onDelete: 'CASCADE'});
    };

    Poi.__SETTINGS = __SETTINGS;

    return Poi;
};

module.exports = poi;
