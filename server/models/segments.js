const {PREFIX, SEGMENT_STATUS} = require('./config');
const Segments = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: SEGMENT_STATUS,
        vegetation_status: {},
    };
    const Segments = sequelize.define(`segments`, {
        uploads: {
            type: DataTypes.VIRTUAL,
        },
        title: DataTypes.STRING,
        comment: DataTypes.STRING,
        description: DataTypes.STRING,
        // nazwa_linii: DataTypes.STRING,
        nazwa_ciagu_id: DataTypes.STRING,
        // NAZWA_TAB: DataTypes.STRING,
        przeslo: DataTypes.STRING,

        status: {
            type: DataTypes.STRING,
            defaultValue: __SETTINGS.STATUS.SERVICE
        },
        vegetation_status: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            validate: {
                max: 100,
                min: 0
            }
        },
        distance_lateral: {
            type: DataTypes.INTEGER,
            validate: {
                max: 10,
                min: 0
            }
        },
        distance_bottom: {
            type: DataTypes.INTEGER,
            validate: {
                max: 15,
                min: 0
            }
        },
        shutdown_time: {
            type: DataTypes.INTEGER,
            validate: {
                max: 12,
                min: 1
            }
        },
        track: {
            type: DataTypes.INTEGER,
            validate: {
                max: 2,
                min: 1
            }
        },
        operation_type: {
            type: DataTypes.STRING
        },
        time_of_operation: {
            type: DataTypes.INTEGER,
            validate: {
                max: 12,
                min: 1
            }
        },
        time_for_next_entry: {
            type: DataTypes.STRING
        },
        // permit_document: {
        //     type: DataTypes.STRING
        // },
        parcel_number_for_permit: {
            type: DataTypes.INTEGER
        },
        notes: {
            type: DataTypes.STRING
        },


        powerLineId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        points: DataTypes.GEOMETRY,

    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Segments.associate = models => {
        Segments.belongsTo(models.Users, {foreignKey: 'userId', onDelete: 'CASCADE'});
        Segments.belongsTo(models.Powerlines, {foreignKey: 'powerLineId', onDelete: 'CASCADE'});
    };

    Segments.__SETTINGS = __SETTINGS;

    return Segments;
}

module.exports = Segments;
