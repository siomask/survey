const Sequelize = require('sequelize');
const config = require('../config');
const {PREFIX, OBJECT_TABLE_NAME} = require('./config');
const Op = Sequelize.Op;
//console.log(111111111111, config.db.postgress.DB_HOST);
const sequelize = new Sequelize(`postgres://${config.db.postgress.DB_USER}:${config.db.postgress.DB_PSW}@${config.db.postgress.DB_HOST}:5432/${config.db.postgress.DB_NAME}`);
// sequelize.query(
//         `
//         CREATE EXTENSION postgis ;`
//         );
sequelize.createSchema(PREFIX);

const models = {
    Users: sequelize.import('./user'),
    Projects: sequelize.import('./projects'),
    Poles: sequelize.import('./poles'),
    Category: sequelize.import('./category'),
    Segments: sequelize.import('./segments'),
    Stations: sequelize.import('./stations'),
    [OBJECT_TABLE_NAME.POI]: sequelize.import('./poi'),
    Logs: sequelize.import('./logs'),
    Powerlines: sequelize.import('./powerlines'),
    Uploads: sequelize.import('./uploads'),
    // PoleSegments: sequelize.import('./pole_segments'),
    ProjectPowerline: sequelize.import('./project_powerline'),
    Parcels: sequelize.import('./parcels'),
    [OBJECT_TABLE_NAME.USER_PROJECTS]: sequelize.import('./user_projects'),
    [OBJECT_TABLE_NAME.PARCEL_UPLOADS]: sequelize.import('./parcel_uploads'),
    [OBJECT_TABLE_NAME.POLE_UPLOADS]: sequelize.import('./pole_uploads'),
    [OBJECT_TABLE_NAME.SEGMENT_UPLOADS]: sequelize.import('./segment_uploads'),
    [OBJECT_TABLE_NAME.STATION_UPLOADS]: sequelize.import('./station_uploads'),
    [OBJECT_TABLE_NAME.POI_UPLOADS]: sequelize.import('./poi_uploads'),
};

Object.keys(models).forEach(key => {
    if ('associate' in models[key]) {
        models[key].associate(models);
    }
});

module.exports = {
    Op,
    models,
    sequelize,
    OBJECT_TABLE_NAME
};
