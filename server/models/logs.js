const {PREFIX} = require('./config');
const logs = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const Logs = sequelize.define(`logs`, {
        action: DataTypes.STRING,
        ip: DataTypes.STRING,
        description: DataTypes.STRING(1500),
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Logs.associate = models => {
        Logs.belongsTo(models.Users, {foreignKey: 'userId',onDelete: 'CASCADE'});
    };

    Logs.__SETTINGS = __SETTINGS;
    return Logs;
}

module.exports = logs;
