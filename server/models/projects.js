const {PREFIX} = require('./config');
const projects = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const Projects = sequelize.define(`projects`, {
        title: DataTypes.STRING,
        contractor: DataTypes.STRING,
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },

    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Projects.associate = models => {
        // Projects.belongsTo(models.Users, {foreignKey: 'userId',onDelete: 'CASCADE'});
        Projects.hasMany(models.Poles, {onDelete: 'CASCADE'});
        Projects.hasMany(models.Segments, {onDelete: 'CASCADE'});
    };

    Projects.__SETTINGS = __SETTINGS;

    return Projects;
}

module.exports = projects;
