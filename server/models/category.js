const {PREFIX, STATUS} = require('./config');
const category = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS,
    };
    const Category = sequelize.define(`categories`, {
        title: DataTypes.STRING,
        comment: DataTypes.STRING,
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Category.associate = models => {
        Category.belongsTo(models.Users, {foreignKey: 'userId', onDelete: 'CASCADE'});
    };

    Category.__SETTINGS = __SETTINGS;

    return Category;
}

module.exports = category;
