const {PREFIX} = require('./config');
const StationUploads = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const StationUploads = sequelize.define(`station_uploads`, {
        stationId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        uploadId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },
    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX

    });

    StationUploads.associate = models => {
        models.Stations.belongsToMany(models.Uploads, {through: StationUploads,as: 'StationUploads', foreignKey: 'stationId',onDelete: 'CASCADE'});
        models.Uploads.belongsToMany(models.Stations, {through: StationUploads,as: 'StationUploads', foreignKey: 'uploadId',onDelete: 'CASCADE'});
    };
    StationUploads.__SETTINGS = __SETTINGS;

    return StationUploads;
}

module.exports = StationUploads;
