const {PREFIX, STATUS} = require('./config');
const Uploads = (sequelize, DataTypes) => {
    const __SETTINGS = {
        TYPE: {
            SIMPLE: 1
        },
        STATUS,
    };
    const Uploads = sequelize.define(`uploads`, {

        path: DataTypes.STRING,
        metada: DataTypes.STRING,

        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        }

    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Uploads.associate = models => {
        Uploads.belongsTo(models.Users, {foreignKey: 'userId',onDelete: 'CASCADE'});
    };
    Uploads.__SETTINGS = __SETTINGS;

    return Uploads;
}

module.exports = Uploads;
