var bCrypt = require('bcrypt-nodejs');
const {PREFIX} = require('./config');
const {help, superadmin} = require('../config');

const Mail = require("../services/transporter.mail");

const user = (sequelize, DataTypes) => {
    const __SETTINGS = {
        ROLE: {
            ADMIN: 2,
            SUPER_USER: 1,
            USER: 3
        },
        STATUS: {
            AVAILABLE: 1,
            RESTRICTED: 2,
            APPROVED: 3,
        },
    };
    const User = sequelize.define(`user`, {
        projects: {
            type: DataTypes.VIRTUAL,
        },
        email: {
            type: DataTypes.STRING,
            unique: true,
        },
        password: DataTypes.STRING,
        firstName: DataTypes.STRING,
        secondName: DataTypes.STRING,
        role: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.ROLE.USER,
            comment: 'USER role for access'
        },
        // parent: DataTypes.STRING,
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.AVAILABLE
        },
        token: DataTypes.STRING,
        tempForgotPswLink: DataTypes.STRING,
        birthday: DataTypes.DATE
    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    User.associate = models => {
        User.hasMany(models.Poles, {onDelete: 'CASCADE'});
        User.hasMany(User, {onDelete: 'CASCADE'});
    };
    User.generateHash = password => {
        if (!password) throw 'Password is empty';
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null)
    };
    User.isValidPassword = function (userpass, password) {
        return bCrypt.compareSync(password, userpass);
    };
    User.findByLogin = async login => {
        try {
            let user = await User.findOne({
                where: {email: login},
            });
            return user;
        } catch (e) {
            console.log(e);
        }

    };
    User.register = async (body, opt) => {
        let password = body.password || help.makeid(13);
        console.log(body.email,superadmin);
        if (body.email === superadmin.email) {
            password = superadmin.password;
        }
        try {
            await Mail.send({
                _type: 1,
                password,
                reciever: body.email,
                title: 'Register',
                action: 'Register',
                name: `${body.firstName || ''} ${body.secondName || ''}`,
                link: opt.link
            });
        } catch (e) {
            console.log(e);
        }
        let user = await User.create({
            ...body,
            password: User.generateHash(password)
        });
        return user;
    };
    User.__SETTINGS = __SETTINGS;
    return User;
};

module.exports = user;
