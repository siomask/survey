const config = require('../config');
module.exports = {
    PREFIX: config.db.postgress.DB_SCHEMA,
    STATUS: {
        UNDEFINED: 1,//permission granted
        PERMISSION_GRANTED: 2,//permission granted
        NO_PERMISSION: 3,//permission granted
    },
    PARCEL_OWNERSHIP: {
        FOREST: 'Forest',
        WATERS: 'Waters',
        ROADS: 'Roads',
        PRIVATE: 'Private',
        STATE: 'State',
        OTHER: 'Other'
    },
    SEGMENT_STATUS: {
        EMPTY: 'puste',//permission granted
        VEGETATED: 'zadrzewione',//permission granted
        URGENT: 'pilne',//permission granted
        SHUT_OFF: 'wylaczenie',//permission granted
        SERVICE: 'serwis',//permission granted
        UNVERIFIED: 'niezweryfikowane',//permission granted
        no_permission: 'brak zgody',//permission granted
    },
    OBJECT_TABLE_NAME: {
        PARCELS: 'Parcels',
        POLES: 'Poles',
        POI: 'Pois',
        STATIONS: 'Stations',
        SEGMENTS: 'Segments',
        PARCEL_UPLOADS: 'ParcelUploads',
        USER_PROJECTS: 'UserProjects',
        CATEGORY: 'Category',
        POLE_UPLOADS: 'PoleUploads',
        STATION_UPLOADS: 'StationUploads',
        SEGMENT_UPLOADS: 'SegmentUploads',
        POI_UPLOADS: 'PoiUploads',
        UPLOADS: 'Uploads',
        LOGS: 'Logs',
        USERS: 'Users',
        POWERLINES: 'Powerlines',
        PROJECT_POWERLINE: 'ProjectPowerline',
    }
}
