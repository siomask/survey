const {sequelize, models} = require('./index');
const config = require('../config');
const {PREFIX, OBJECT_TABLE_NAME} = require('./config');

const copySql = `
INSERT INTO ${PREFIX}.parcels (  userId, projectId,points)
 SELECT 1 ,1 , geom  as points
FROM projekty.parcels LIMIT 1
;

`;
/*sequelize.query(copySql).then((res) => {
    console.log(res);
}).catch((e) => {
    console.log(e);
});*/

const count = 100000;
const PowerLines = {};
const Projects = {};
const Project_PowerLine = {};
const list = [
    {
        from: `Select  kontrahent as contractor, nazwa as title, id   from projekty.zlecenia `,
        toName: 'projects',
        to: models.Projects
    },

    /*{
        from: `SELECT DISTINCT powerline as title FROM  from projekty.parcels `,
        toName: 'powerline',
        to: models.Powerlines
    },*/
    {
        from: `Select * , ST_AsGeoJSON(ST_Transform(geom,4326)) As points from projekty.parcels_new   LIMIT ${count}`,
        toName: 'parcels',
        to: models.Parcels
    },
    {
        from: `Select *, ST_AsGeoJSON(ST_Transform(geom,4326)) As points from projekty.linie_zadania_gotowe   LIMIT ${count}`,
        toName: 'segments',
        to: models.Segments
    },
    {
        from: `Select * ,  ST_AsGeoJSON(ST_Transform(geom,4326)) As points from projekty.poles   LIMIT ${count}`,
        toName: 'poles',
        to: models.Poles
    },
    {
        from: `Select * ,  ST_AsGeoJSON(ST_Transform(geom,4326)) As points from projekty.stations   LIMIT ${count}`,
        toName: 'stations',
        to: models.Stations
    }
];

let mainUser;
const Sequelize = require('sequelize');

const _sequelize = new Sequelize(`postgres://${config.db.postgress.DB_USER}:${config.db.postgress.DB_PSW}@${config.db.postgress.DB_HOST}:5432/surveyapp`);

function copyEls(list) {
    if (!list.length) return;
    const _sql = list.shift();
    _sequelize.query(_sql.from).then(async (res) => {
        res = res[0];
        /*if (_sql.toName === 'powerline') {
            for (let i = 0; i < res.length; i++) {
                const record = await _sql.to.create({
                    ...res[i],
                    userId: 1,
                });
                PowerLines[res[i].title] = record.id;
            }
        } else*/
        if (_sql.toName === 'projects') {
            // let sql = `INSERT INTO ${PREFIX}.${_sql.toName} ( id, contractor, title,userId ) VALUES`;
            // for (let i = 0; i < res.length; i++) {
            //     sql += `(${res[i].id},${res[i].contractor},${res[i].title},1),`;
            //
            // }
            // sql = sql.substr(0, sql.length - 1);
            // return sequelize.query(sql).then(()=> copyEls(list)).catch((er)=>{
            //     console.log(er);
            // })

            if (!mainUser) {

                mainUser = await models.Users.findOne({email: config.superadmin.email});
                if(!mainUser)await models.Users.register({
                    ...config.superadmin,
                    role: models.Users.__SETTINGS.ROLE.SUPER_USER
                }, {
                    link: 'resr'
                });
                mainUser = await models.Users.findOne({email: config.superadmin.email});
            }
            for (let i = 0; i < res.length; i++) {
                const record = await _sql.to.create({
                    ...res[i],
                    // userId: 1,
                });
                Projects[record.id] = record;
                const method = `add${OBJECT_TABLE_NAME.USER_PROJECTS}`;
                mainUser[method](record.id);
            }
        } else {
            for (let i = 0; i < res.length; i++) {
                const rowItem = {
                    ...res[i]
                };
                rowItem.id = null;
                delete rowItem.id;
                const newRecord = {
                    ...rowItem,
                    points: res[i].points,
                    userId: 1,
                    projectId: res[i].ID_ZLECENIA || res[i].project || 3449
                };
                if (rowItem.powerline) {
                    if (!PowerLines[rowItem.powerline]) {
                        const record = await models.Powerlines.create({
                            title: rowItem.powerline,
                            userId: 1,
                            projectId: res[i].ID_ZLECENIA || res[i].project || 3449
                        });
                        PowerLines[rowItem.powerline] = record;
                        console.log("Powerline-----", record.id);

                    }


                    if (!Project_PowerLine[`${newRecord.projectId}_${PowerLines[rowItem.powerline].id}`]) {
                        Project_PowerLine[`${newRecord.projectId}_${PowerLines[rowItem.powerline].id}`] = true;
                        Projects[newRecord.projectId].addProjectPowerline(PowerLines[rowItem.powerline]);
                    }
                    newRecord.powerLineId = PowerLines[rowItem.powerline].id;
                }

                await _sql.to.create(newRecord)
            }
        }

        copyEls(list);
    }).catch((e) => {
        console.log(e);
    });
}

copyEls(list);
