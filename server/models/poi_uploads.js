const {PREFIX,OBJECT_TABLE_NAME} = require('./config');
const PoiUploads = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const PoiUploads = sequelize.define(`poi_uploads`, {
        poiId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        uploadId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },
    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX

    });

    PoiUploads.associate = models => {
        models[OBJECT_TABLE_NAME.POI].belongsToMany(models.Uploads, {
            through: PoiUploads,
            as: OBJECT_TABLE_NAME.POI_UPLOADS,
            foreignKey: 'poiId',
            onDelete: 'CASCADE'
        });
        models.Uploads.belongsToMany(models[OBJECT_TABLE_NAME.POI], {
            through: PoiUploads,
            as: OBJECT_TABLE_NAME.POI_UPLOADS,
            foreignKey: 'uploadId',
            onDelete: 'CASCADE'
        });
    };
    PoiUploads.__SETTINGS = __SETTINGS;

    return PoiUploads;
}

module.exports = PoiUploads;
