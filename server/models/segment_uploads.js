const {PREFIX} = require('./config');
const SegmentUploads = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const SegmentUploads = sequelize.define(`segment_uploads`, {
        segmentId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        uploadId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },
    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX

    });

    SegmentUploads.associate = models => {
        models.Segments.belongsToMany(models.Uploads, {
            through: SegmentUploads,
            as: 'SegmentUploads',
            foreignKey: 'segmentId',onDelete: 'CASCADE'
        });
        models.Uploads.belongsToMany(models.Segments, {
            through: SegmentUploads,
            as: 'SegmentUploads',
            foreignKey: 'uploadId',onDelete: 'CASCADE'
        });
    };
    SegmentUploads.__SETTINGS = __SETTINGS;

    return SegmentUploads;
}

module.exports = SegmentUploads;
