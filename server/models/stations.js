const {PREFIX, STATUS} = require('./config');
const Stations = (sequelize, DataTypes) => {
    const __SETTINGS = {
        TYPE: {
            SIMPLE: 1
        },
        STATUS,
    };
    const Stations = sequelize.define(`stations`, {
        uploads: {
            type: DataTypes.VIRTUAL,
        },
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        nazw_stac: DataTypes.STRING,
        num_eksp_s: DataTypes.STRING,
        comment: DataTypes.STRING,

        type: DataTypes.INTEGER,
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.UNDEFINED
        },

        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        points: DataTypes.GEOMETRY,

    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Stations.associate = models => {
        Stations.belongsTo(models.Users, {foreignKey: 'userId',onDelete: 'CASCADE'});
        Stations.belongsTo(models.Projects, {foreignKey: 'projectId',onDelete: 'CASCADE'});
    };
    Stations.__SETTINGS = __SETTINGS;

    return Stations;
}

module.exports = Stations;
