const {PREFIX, STATUS} = require('./config');
const poles = (sequelize, DataTypes) => {
    const __SETTINGS = {
        TYPE: {
            SIMPLE: 1
        },
        STATUS,
    };
    const Poles = sequelize.define(`poles`, {
        uploads: {
            type: DataTypes.VIRTUAL,
        },
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        comment: DataTypes.STRING,
        type: DataTypes.INTEGER,
        num_slup: DataTypes.STRING,
        layer: DataTypes.STRING,

        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.UNDEFINED
        },
        powerLineId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        points: DataTypes.GEOMETRY,

    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Poles.associate = models => {
        Poles.belongsTo(models.Users, {foreignKey: 'userId',onDelete: 'CASCADE'});
        Poles.belongsTo(models.Projects, {foreignKey: 'projectId',onDelete: 'CASCADE'});
        Poles.belongsTo(models.Powerlines, {foreignKey: 'powerLineId',onDelete: 'CASCADE'});
    };
    Poles.__SETTINGS = __SETTINGS;

    return Poles;
}

module.exports = poles;
