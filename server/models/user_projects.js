const {PREFIX, OBJECT_TABLE_NAME} = require('./config');
const UserProjects = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const UserProjects = sequelize.define(`user_projects`, {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },
    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    UserProjects.associate = models => {
        models.Users.belongsToMany(models.Projects, {
            through: UserProjects,
            as: OBJECT_TABLE_NAME.USER_PROJECTS,
            foreignKey: 'userId',
            onDelete: 'NO ACTION'
        });
        models.Projects.belongsToMany(models.Users, {
            through: UserProjects,
            as: OBJECT_TABLE_NAME.USER_PROJECTS,
            foreignKey: 'projectId',
            onDelete: 'NO ACTION'
        });
    };
    UserProjects.__SETTINGS = __SETTINGS;

    return UserProjects;
};

module.exports = UserProjects;
