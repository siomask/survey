const {PREFIX} = require('./config');
const ProjectPowerline = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const ProjectPowerline = sequelize.define(`project_powerline`, {
        powerlineId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        comment: DataTypes.STRING,
        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },
    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    ProjectPowerline.associate = models => {
        models.Powerlines.belongsToMany(models.Projects, {
            through: ProjectPowerline,
            as: 'ProjectPowerline',
            foreignKey: 'powerlineId',onDelete: 'CASCADE'
        });
        models.Projects.belongsToMany(models.Powerlines, {
            through: ProjectPowerline,
            as: 'ProjectPowerline',
            foreignKey: 'projectId',onDelete: 'CASCADE'
        });
    };
    ProjectPowerline.__SETTINGS = __SETTINGS;

    return ProjectPowerline;
};

module.exports = ProjectPowerline;
