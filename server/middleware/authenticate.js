const passport = require('passport');
const {models} = require('../models');

const auth = async (req, res, next) => {
    try {
        var token = req.headers['authorization'] || req.headers['Authorization'];
        if (!token) throw 'no token provided';
        const user = await models.Users.findOne({where: {token: token}});
        if (!user) throw 'Token is not valid';
        req.user = user;
        req.userId = user.id;
        next();
    } catch (e) {
        return res.status(401).json({
            status: false,
            code: 2,
            message: e.message || e,
        });
    }

};
module.exports = {
    auth,
    admin: (req, res, next) => {
        if (req.user.role < models.Users.__SETTINGS.ROLE.USER) {
            next()
        } else {
            return res.status(401).json({
                status: false,
                message: 'Access denied!',
            });
        }
    }
};
