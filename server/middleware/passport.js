const jwt = require("jwt-simple");

const LocalStrategy = require("passport-local").Strategy;
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;

const config = require("../config");

const {models} = require("../models");
const User = models.Users;


module.exports = function (passport) {
    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(async function (user, done) {
        const id = typeof user === 'object' ? user.id : user;
        try {
            done(null, await User.findByPk(id));
        } catch (e) {
            done(e);
        }
    });

    passport.use(new LocalStrategy({
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true
    }, function (req, email, password, done) {
        User.findByLogin(email).then(function (user) {
            // if (err) {
            //     return done(err);
            // }

            if (!user) {
                return done(null, false, {
                    status: false,
                    message: "No user found."
                });
            }
            // if (!user.active) {
            //   return done(null, false, {
            //     status: false,
            //     message: "User is not active"
            //   });
            // }

            if (!User.isValidPassword(user.password, password)) {

                return done(null, false, {
                    status: false,
                    message: "Oops! Wrong password."
                });
            }

            // user.token = "JWT " + jwt.encode({_id: user._id, email: user.email}, config.security.secret);
            if (req.session) req.session.user = user;
            if (user.token) {
                return done(null, user, {
                    status: true,
                    message: "Login success."
                });
            }
            user.update({
                token: "JWT " + jwt.encode({
                    _id: user._id,
                    email: user.email,
                    exired: Date.now() + 1000 * 60 * 60 * 24 * 30
                }, config.security.secret)
            }).then(function (user) {
                done(null, user, {
                    status: true,
                    message: "Login success."
                });
            });
        }).catch(done);
    }));

    passport.use(new JwtStrategy({
        secretOrKey: config.security.secret,
        jwtFromRequest: ExtractJwt.fromAuthHeader()
    }, function (payload, done) {
        User.findByPk(payload._id).then(function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false, {
                    status: false,
                    message: "Failed to authenticate token."
                });
            }

            done(null, user, {
                status: true,
                message: "Authenticate success."
            });
        });
    }));
};
