const node_ssh = require('node-ssh');
const ssh = new node_ssh();

const config = {
    host: process.env.SERVER_HOST,
    port: '22',
    username: process.env.SERVER_USER,
    password: process.env.SERVER_PSW
};
deploy();

async function deploy() {
    try {
        ssh.connect(config).then(() => {
            ssh.execCommand(`cd /root/surveyingapp && git reset --hard && git checkout master && git pull origin master &&  npm install && npm run build  && pm2 restart 0`, {}).then(function (result) {
                console.log('STDOUT: ' + result.stdout);
                console.log('STDERR: ' + result.stderr)
                ssh.dispose();
            })
        });


    } catch (err) {
        console.log(err)
    }
}
