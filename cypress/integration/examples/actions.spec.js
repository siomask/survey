/// <reference types="Cypress" />

context('Actions', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000')
    })

    // https://on.cypress.io/interacting-with-elements

    it('mouse move', () => {
        cy.get('.preloader').should('have.class', 'hide').then(() => {
            cy.window().trigger('mousedown', { which: 1 });
            cy.wait(1000);
            cy.window().trigger('mousemove', { position:'topRight',duration: 2000 })
                .trigger('mouseup', { force: true })
                .trigger('wheel', { force: true })
        })

    })

})
