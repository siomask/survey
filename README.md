This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `npm install yarn -g`
### `yarn install`

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!
 
### `npm run server`

run local server on [http://localhost:3009](http://localhost:3009)


### TO reset db
 1)edit file server/index.js on line 108 to const settings = {force: process.env.NODE_ENV === 'development'};  
 2)npm run server  
 3)node server/models/migration


### TO deploy project
 !NOTE server/index.js force:false!
 1) Connect to remote server: ssh root@116.203.190.27
 2) cd surveyingapp
 2) git pull origin master
 3) npm install 
 4) npm run build
 5) pm2 restart 0
 